import PropTypes from "prop-types";
import React from "react";

const SectionTitleWithText = ({ spaceTopClass, spaceBottomClass }) => {
  return (
    <div
      className={`welcome-area ${spaceTopClass ? spaceTopClass : ""} ${
        spaceBottomClass ? spaceBottomClass : ""
      }`}
    >
      <div className="container">
        <div className="welcome-content text-center">
          <h5>Who Are We</h5>
          <h1>Welcome To Mags</h1>
          <p>
            Mags is one of the international fashion companies.
            The customer is at the heart of our unique business model,
            which includes design, production, distribution and sales through our
            extensive retail network.We believe in a world where you have total freedom to be you,
            without judgement. To experiment. To express yourself. To be brave and grab life as the
            extraordinary adventure it is. So we make sure everyone has an equal chance to discover all
            the amazing things they’re capable of – no matter who they are, where they’re from
            or how they look. We exist to give you the confidence to be whoever you want to be.
            {" "}
          </p>
        </div>
      </div>
    </div>
  );
};

SectionTitleWithText.propTypes = {
  spaceBottomClass: PropTypes.string,
  spaceTopClass: PropTypes.string
};

export default SectionTitleWithText;
