import PropTypes from "prop-types";
import React from "react";

const HeaderTopTwo = ({borderStyle}) => {
    return (
        <div
            className={`header-top-wap ${
                borderStyle === "fluid-border" ? "border-bottom" : ""
            }`} style={{backgroundColor: "#EB3E3E"}}>
                    <div className="bannerText">
                    We deliver within 2 - 3 days within Colombo, out of Colombo within 3 - 5 working days.
                    Whatsapp {" "}
                    +94 77 344 4602{" "}
                    to check the status of your order{" "}
                    0770024007{" "}
                </div>
        </div>
    );
};

HeaderTopTwo.propTypes = {
    borderStyle: PropTypes.string,
};



export default HeaderTopTwo;
